﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GTLD : MonoBehaviour
{

    private Text domainnametext,ownernametext, ownerorgtext, registrartext, owneremailtext, adminemailtext, lockedtext, quaranainetxt, nameservertext, dnssectext, deletiontxt;
    

    void Start()
    {
        ownernametext = GameObject.Find("Ownertxt").GetComponent<Text>();
        ownerorgtext = GameObject.Find("Ownerorgtxt").GetComponent<Text>();
        domainnametext = GameObject.Find("Domainnametxt").GetComponent<Text>();
        registrartext = GameObject.Find("Registrartxt").GetComponent<Text>();
        owneremailtext = GameObject.Find("Ownermailtxt").GetComponent<Text>();
        adminemailtext = GameObject.Find("Adminmailtxt").GetComponent<Text>();
        lockedtext = GameObject.Find("Locktxt").GetComponent<Text>();
        quaranainetxt = GameObject.Find("Quarantainetxt").GetComponent<Text>();
        deletiontxt = GameObject.Find("Einddatumtxt").GetComponent<Text>();
        dnssectext = GameObject.Find("DNSSECtxt").GetComponent<Text>();
        nameservertext = GameObject.Find("Nameservertxt").GetComponent<Text>();


        
    }


    public void Update()
    {
        
        string[] linesInFile = INIT.whoistextshort.Split('\n');
        foreach (string line in linesInFile)
        {
            if (line.Contains("No match for") == true)
            {
                ownernametext.text = "Jonguh, dit domein is niet geregistreerd";
                string Domainfree = line.Replace("No match for", "");
                Domainfree = Domainfree.Replace(".","");
                Domainfree = Domainfree.TrimEnd();
                Domainfree = Domainfree.TrimStart();
                domainnametext.text = Domainfree;
            }

            if (line.Contains("Registrant Name:") == true)

            {
                string Owner = line.Replace("Registrant Name:", "");
                Owner = Owner.TrimEnd();
                Owner = Owner.TrimStart();
                ownernametext.text = Owner;
            }

            if (line.Contains("Registrant Organization:") == true)

            {
                string Org = line.Replace("Registrant Organization:", "");
                Org = Org.TrimEnd();
                Org = Org.TrimStart();
                ownerorgtext.text = Org;
            }

            if (line.Contains("Domain Name:") == true)

            {
                string Domainname = line.Replace("Domain Name:", "");
                Domainname = Domainname.TrimEnd();
                Domainname = Domainname.TrimStart();
                domainnametext.text = Domainname;
            }

            if (line.Contains("Registrar:") == true)

            {
                string Reg = line.Replace("Registrar:", "");
                Reg = Reg.TrimEnd();
                Reg = Reg.TrimStart();
                registrartext.text = Reg;
            }

            if (line.Contains("Registrant Email:") == true)

            {
                string Ownermail = line.Replace("Registrant Email:", "");
                Ownermail = Ownermail.TrimEnd();
                Ownermail = Ownermail.TrimStart();
                owneremailtext.text = Ownermail;
            }

            if (line.Contains("Admin Email:") == true)

            {
                string Adminmail = line.Replace("Admin Email:", "");
                Adminmail = Adminmail.TrimEnd();
                Adminmail = Adminmail.TrimStart();
                adminemailtext.text = Adminmail;
            }

            if (line.Contains("icann.org/epp#clientTransferProhibited") == true)

            {
                string Locked = ("Domein is locked , kan NIET verhuisd worden");
                lockedtext.text = Locked;
            }
            if (line.Contains("icann.org/epp#ok") == true)

            {
                string Locked = ("Domein is unlocked, kan dus verhuisd worden");
                lockedtext.text = Locked;
            }


            if (line.Contains("") == true)

            {
                string Quaraniane = ("ikmoetdezenogfixen");
                Quaraniane = Quaraniane.TrimEnd();
                Quaraniane = Quaraniane.TrimStart();
                quaranainetxt.text = Quaraniane;
            }
            if (line.Contains("Registrar Registration Expiration Date:") == true)

            {
                string Delete = line.Replace("Registrar Registration Expiration Date:","");
                Delete = Delete.TrimEnd();
                Delete = Delete.TrimStart();
                deletiontxt.text = Delete;
            }
            if (line.Contains("DNSSEC:") == true)

            {
                string DNSSEC = line.Replace("DNSSEC:", "");
                DNSSEC = DNSSEC.TrimEnd();
                DNSSEC = DNSSEC.TrimStart();
                dnssectext.text = DNSSEC;
            }
            if (line.Contains("Name Server:") == true)

            {
                string Nameserver = line.Replace("Name Server:", "");
                Nameserver = Nameserver.TrimEnd();
                Nameserver = Nameserver.TrimStart();
                nameservertext.text = Nameserver;
            }
        }
    }
}
