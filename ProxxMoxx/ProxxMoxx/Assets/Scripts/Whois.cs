﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace DomainTools
{
    /// <summary>
    /// A class to lookup whois information.
    /// </summary>
    /// 

    /// <summary>
    /// Retrieves whois information
    /// </summary>
    /// <param name="domainName">The registrar or domain or name server whose whois information to be retrieved</param>
    /// <param name="recordType">The type of record i.e a domain, nameserver or a registrar</param>whois.verisign-grs.com
    /// <returns></returns>
   

    public class Whois
    {
        private const int Whois_Server_Default_PortNumber = 43;
        private const string Domain_Record_Type = "";
        public static string Whois_Server = "whois.verisign-grs.com";
                
        public static string Lookup(string domainName)
        {
            using (TcpClient whoisClient = new TcpClient())
            {
                whoisClient.Connect(Whois_Server, Whois_Server_Default_PortNumber);

                string domainQuery = Domain_Record_Type + "" + domainName + "\r\n";
                byte[] domainQueryBytes = Encoding.ASCII.GetBytes(domainQuery.ToCharArray());

                Stream whoisStream = whoisClient.GetStream();
                whoisStream.Write(domainQueryBytes, 0, domainQueryBytes.Length);

                StreamReader whoisStreamReader = new StreamReader(whoisClient.GetStream(), Encoding.ASCII);

                string streamOutputContent = "";
                List<string> whoisData = new List<string>();
                while (null != (streamOutputContent = whoisStreamReader.ReadLine()))
                {
                    whoisData.Add(streamOutputContent);
                }

                whoisClient.Close();
                string[] whoisdata = whoisData.ToArray();
                return String.Join(Environment.NewLine,whoisdata);
            }
        }
    }
}