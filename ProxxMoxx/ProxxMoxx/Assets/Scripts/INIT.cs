﻿﻿﻿using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;



public class INIT : MonoBehaviour
{

    private Text domainnametext, ownernametext, ownerorgtext, registrartext, owneremailtext, adminemailtext, lockedtext, quaranainetxt, nameservertext, dnssectext, deletiontxt;
    public InputField domainnameFIELD;
    public Button whoisbutton, resetbutton;
    public string result = "no result";
    private GameObject whoisFull, whoisShort;
    public Text whoistext;
    public Toggle Shortwhois;
    public static string whoistextshort;


    // Use this for initialization
    void Start()
    {
        whoisShort = GameObject.FindWithTag("quick");
        whoisFull = GameObject.FindWithTag("full");
        Button whois = whoisbutton.GetComponent<Button>();
        Button reset = resetbutton.GetComponent<Button>();
        whois.onClick.AddListener(run_checkdomain);
        reset.onClick.AddListener(reset_checkdomain);

        ownernametext = GameObject.Find("Ownertxt").GetComponent<Text>();
        ownerorgtext = GameObject.Find("Ownerorgtxt").GetComponent<Text>();
        domainnametext = GameObject.Find("Domainnametxt").GetComponent<Text>();
        registrartext = GameObject.Find("Registrartxt").GetComponent<Text>();
        owneremailtext = GameObject.Find("Ownermailtxt").GetComponent<Text>();
        adminemailtext = GameObject.Find("Adminmailtxt").GetComponent<Text>();
        lockedtext = GameObject.Find("Locktxt").GetComponent<Text>();
        quaranainetxt = GameObject.Find("Quarantainetxt").GetComponent<Text>();
        deletiontxt = GameObject.Find("Einddatumtxt").GetComponent<Text>();
        dnssectext = GameObject.Find("DNSSECtxt").GetComponent<Text>();
        nameservertext = GameObject.Find("Nameservertxt").GetComponent<Text>();

    }




    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))                     // zoeken met enter toets
            Invoke("run_checkdomain", 0);                         
        if (Shortwhois.isOn == true)                              // bepalen welk bericht wordt weergegeven 
        {
            whoisFull.SetActive(false);
            whoisShort.SetActive(true);
        }else
        {
            whoisFull.SetActive(true);
            whoisShort.SetActive(false);
        }

    }



    void run_checkdomain()
    {                                                              // domainchecker 
        Clearresult();                                             // oude zoekresultaten wissen
        whoistext.text = ("Hakuna your tatas, I'm loading shit");  // omdat bitches ongeduldig zijn
        result = domainnameFIELD.text;                             // omdat domeinnaam nodig is 
        result = result.TrimEnd();                                 // omdat bitches spaties achter domein plaatsen
        result = result.TrimStart();                               // of ervoor plaatsen 
#region Getwhoisserver
        if (result == "")
        {
            whoistext.text = ("Lege velden zoeken niet lekker");
        }
        else if (!result.Contains("."))
        {
            result = (result + ".com");
            DomainTools.Whois.Whois_Server = "whois.verisign-grs.com";
            var whoisText = DomainTools.Whois.Lookup(result);
            whoistext.text = (whoisText);
            Invoke("GetGTLDwhois", 0);
        }
        else if (result.Contains(".com") && !result.Contains(".com.") || result.Contains(".net") && !result.Contains(".net.")) || result.Contains(".tv") && !result.Contains(".tv."))
        {
            DomainTools.Whois.Whois_Server = "whois.verisign-grs.com";
            var whoisText = DomainTools.Whois.Lookup(result);
            whoistext.text = (whoisText);
            Invoke("GetGTLDwhois", 0);
        }
        else
            whoistext.text = ("dit is geen geldig domein of iets anders gaat niet goed, schop Moxxi!");
#endregion            
    }




    void GetGTLDwhois()                                            // omdat .com speciaal wilt zijn 
    {
        string[] linesInFile = whoistext.text.Split('\n');
        foreach (string line in linesInFile)
        {
            if (line.Contains("No match for ") == true)
            {
                whoistextshort = whoistext.text;
            }
            if (line.Contains("Registrar WHOIS Server:") == true)
            {
                string SECONDwhois = line.Replace("Registrar WHOIS Server:", "");
                SECONDwhois = SECONDwhois.TrimEnd();
                SECONDwhois = SECONDwhois.TrimStart();
                DomainTools.Whois.Whois_Server = SECONDwhois;
                Invoke("Stephani_special_whois_GTLD", 0);
            }
        }
    }

    void Stephani_special_whois_GTLD()                             // Uitslag GTLD whois
    {
        whoistext.text = DomainTools.Whois.Lookup(result);
        whoistextshort = whoistext.text;
    }

    void reset_checkdomain()                                        // OMG RESET KNOP!!
    {
        domainnameFIELD.text = "";
    }

    void Clearresult()
    {
        ownernametext.text = "";
        ownerorgtext.text = "";
        domainnametext.text = "";
        registrartext.text = "";
        owneremailtext.text = "";
        adminemailtext.text = "";
        lockedtext.text = "";
        quaranainetxt.text = "";
        deletiontxt.text = "";
        dnssectext.text = "";
        nameservertext.text = "";


    }

}
